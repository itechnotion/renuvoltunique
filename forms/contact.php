<?php
  $receiving_email_address = 'viral@itechnotion.com';
  //$cc_email = 'prajapatisvapnil@gmail.com';
  $cc_email = '';

  if( file_exists($php_email_form = '../assets/vendor/php-email-form/php-email-form.php' )) {
    include( $php_email_form );
  } else {
    die( 'Unable to load the "PHP Email Form" Library!');
  }

  $contact = new PHP_Email_Form();
  $contact->ajax = true;
  
  // Uncomment below code if you want to use SMTP to send emails. You need to enter your correct SMTP credentials
  // $contact->smtp = array(
  //   'host' => 'example.com',
  //   'username' => 'example',
  //   'password' => 'pass',
  //   'port' => '587'
  // );
  
  $name = $_POST['name'];
  $email = $_POST['email'];
  $subject = $_POST['subject'];
  $message = $_POST['message'];
  
  $message_body = "";
  $message_body .= "<b>Name: </b>".$name;
  $message_body .= "<br/><b>Email: </b>".$email;
  $message_body .= "<br/><b>Message: </b>".$message;
  
  $result = $contact->send_email( $receiving_email_address, $cc_email, $subject,$message_body);
  echo $result;

?>
